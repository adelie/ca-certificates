====================================
 README for CA Certificates Package
====================================
:Authors:
  * **A. Wilcox**, maintainer
:Status:
  Production
:Copyright:
  © 2018 Adélie Linux Team.




Introduction
============

This repository contains the contents of the Adélie Linux ca-certificates
package.  The contents of this repository are used to create the APK package
that ships on all Adélie Linux computers.


Licenses
`````````
The upstream certificate data is part of NSS; it is distributed under the
Mozilla Public License version 2.0.

The manipulation code is all licensed under GPL 2.0.


Changes
```````
Any changes to this repository - additions, removal, or version bumps - must
be reviewed before being pushed to the master branch.  There are no exceptions
to this rule.  For security-sensitive updates, contact the Security Team at
sec-bugs@adelielinux.org.
